/* eslint-disable max-len */
/**
 * Schema.org Structured Data
 * @type {Object}
 * @link https://jsonld.com/
 * @link https://searchengineland.com/schema-markup-structured-data-seo-opportunities-site-type-231077
 * @link https://docs.google.com/spreadsheets/d/1Ed6RmI01rx4UdW40ciWgz2oS_Kx37_-sPi7sba_jC3w/edit#gid=0
 */

const address = {
  '@type': 'PostalAddress',
  addressLocality: 'Coyoacán',
  addressRegion: 'CDMX',
  postalCode: '04450',
  streetAddress: 'Erasmo Castellanos Quinto 127',
}
const sameAs = ['https://www.facebook.com/Darila-M%C3%A9xico-1960324564213522/']

module.exports = function schema(params) {
  const defaults = {
    sameAs,
    address,
    name: 'Darila México',
    title: 'Demo Title',
    url: 'http://dinamo.mx',
    description:
      'Darila México es una empresa que busca contribuir a la economía mexicana mediante la promoción en territorio nacional e internacional de artesanías y productos gourmet con calidad de exportación, elaborados por nuestros artesanos mexicanos. ',
  }
  const options = Object.assign(defaults, params)
  return {
    '@context': 'http://schema.org',
    '@graph': [
      // LocalBusiness
      {
        '@type': 'LocalBusiness',
        address,
        name: options.name,
        description: options.description,
        openingHours: ['Mo-Fr 9:00-19:00'],
        telephone: '55 4140 8714',
        url: options.url,
        logo: {
          '@context': 'http://schema.org',
          '@type': 'ImageObject',
          author: 'DarilaMéxico',
          contentLocation: 'Ciudad de Mexico, Mexico',
          contentUrl: 'http://promociones.darilamexico.com/darila_logo.png',
          description: options.description,
          name: options.name,
        },
        image: 'http://promociones.darilamexico.com/darila_logo.png',
        sameAs,
      },
      // Organization
      {
        '@type': 'Organization',
        name: options.name,
        legalName: options.name,
        url: options.url,
        logo: 'http://promociones.darilamexico.com/darila_logo.png',
        foundingDate: '2009',
        founders: [
          {
            '@type': 'Person',
            name: 'Ramses Reyes',
          },
          {
            '@type': 'Person',
            name: 'Seth Gonzalez',
          },
        ],
        address,
        contactPoint: {
          '@type': 'ContactPoint',
          contactType: 'Sales',
          telephone: '+52 55 4140 8714',
          email: 'tanya@wdinamo.com',
        },
        sameAs,
      },
      {
        '@type': 'WebSite',
        url: options.url,
        name: options.title,
        author: {
          '@type': 'Organization',
          name: options.name,
        },
        description: options.description,
        publisher: options.name,
        // potentialAction: {
        //   '@type': 'SearchAction',
        //   target: 'http://www.example.com/?s={search_term}',
        //   'query-input': 'required name=search_term',
        // },
      },
    ],
  }
}
