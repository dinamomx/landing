/**
 * Una forma de importar buefy sin jalar sus 200kbs de código con la idea de
 * solo toma lo que necesitas.
 *
 * NOTE: El componente Upload está bugeado con ssr https://github.com/buefy/buefy/issues/1024
 */

/* eslint-disable max-len */

import Vue from 'vue'

// import Autocomplete from '~/libs/buefy/components/autocomplete/Autocomplete.vue'
import Checkbox from '~/libs/buefy/components/checkbox/Checkbox.vue'
// import CheckboxButton from '~/libs/buefy/components/checkbox/CheckboxButton.vue'
// import Collapse from '~/libs/buefy/components/collapse/Collapse.vue'
// import Datepicker from '~/libs/buefy/components/datepicker/Datepicker.vue'
// import Dialog from '~/libs/buefy/components/dialog/Dialog.vue'
// import Dropdown from '~/libs/buefy/components/dropdown/Dropdown.vue'
// import DropdownItem from '~/libs/buefy/components/dropdown/DropdownItem.vue'
import Field from '~/libs/buefy/components/field/Field.vue'
import Icon from '~/libs/buefy/components/icon/Icon.vue'
import Input from '~/libs/buefy/components/input/Input.vue'
import Loading from '~/libs/buefy/components/loading/Loading.vue'
// import Message from '~/libs/buefy/components/message/Message.vue'
// import Modal from '~/libs/buefy/components/modal/Modal.vue'
// import Notification from '~/libs/buefy/components/notification/Notification.vue'
// import Pagination from '~/libs/buefy/components/pagination/Pagination.vue'
// import Radio from '~/libs/buefy/components/radio/Radio.vue'
// import RadioButton from '~/libs/buefy/components/radio/RadioButton.vue'
import Select from '~/libs/buefy/components/select/Select.vue'
// import Snackbar from '~/libs/buefy/components/snackbar/Snackbar.vue'
// import Switch from '~/libs/buefy/components/switch/Switch.vue'
// import TabItem from '~/libs/buefy/components/tabs/TabItem.vue'
// import Table from '~/libs/buefy/components/table/TableColumn.vue'
// import TableColumn from '~/libs/buefy/components/table/Table.vue'
// import Tabs from '~/libs/buefy/components/tabs/Tabs.vue'
// import Tag from '~/libs/buefy/components/tag/Tag.vue'
// import Taginput from '~/libs/buefy/components/taginput/Taginput.vue'
// import Taglist from '~/libs/buefy/components/tag/Taglist.vue'
// import Timepicker from '~/libs/buefy/components/timepicker/Timepicker.vue'
import Toast from '~/libs/buefy/components/toast/Toast.vue'
// import Tooltip from '~/libs/buefy/components/tooltip/Tooltip.vue'
// import Upload from '~/libs/buefy/components/upload/Upload.vue'

import config, { setOptions } from '~/libs/buefy/utils/config'

const options = {
  defaultIconPack: 'fal',
}
setOptions(Object.assign(config, options))

// function openDialog(propsData) {
//   const vm = Vue
//   const DialogComponent = vm.extend(Dialog)
//   return new DialogComponent({
//     el: document.createElement('div'),
//     propsData,
//   })
// }

// const DialogProgrammatic = {
//   alert(params) {
//     let message
//     if (typeof params === 'string') message = params
//     const defaultParam = {
//       canCancel: false,
//       message,
//     }
//     const propsData = Object.assign(defaultParam, params)
//     return openDialog(propsData)
//   },
//   confirm(params) {
//     const defaultParam = {}
//     const propsData = Object.assign(defaultParam, params)
//     return openDialog(propsData)
//   },
//   prompt(params) {
//     const defaultParam = {
//       hasInput: true,
//       confirmText: 'Done',
//     }
//     const propsData = Object.assign(defaultParam, params)
//     return openDialog(propsData)
//   },
// }

// const LoadingProgrammatic = {
//   open(params) {
//     const defaultParam = {
//       programmatic: true,
//     }
//     const propsData = Object.assign(defaultParam, params)

//     const vm = Vue
//     const LoadingComponent = vm.extend(Loading)
//     return new LoadingComponent({
//       el: document.createElement('div'),
//       propsData,
//     })
//   },
// }

// const ModalProgrammatic = {
//   open(params) {
//     let content
//     let parent
//     if (typeof params === 'string') content = params

//     const defaultParam = {
//       programmatic: true,
//       content,
//     }
//     if (params.parent) {
//       // eslint-disable-next-line  prefer-destructuring
//       parent = params.parent
//       delete params.parent
//     }
//     const propsData = Object.assign(defaultParam, params)

//     const vm = Vue
//     const ModalComponent = vm.extend(Modal)
//     return new ModalComponent({
//       parent,
//       el: document.createElement('div'),
//       propsData,
//     })
//   },
// }

const ToastProgrammatic = {
  open(params) {
    let message
    if (typeof params === 'string') message = params

    const defaultParam = { message }
    const propsData = Object.assign(defaultParam, params)

    const vm = typeof window !== 'undefined' && window.Vue ? window.Vue : Vue
    const ToastComponent = vm.extend(Toast)
    return new ToastComponent({
      el: document.createElement('div'),
      propsData,
    })
  },
}

// const SnackbarProgrammatic = {
//   open(params) {
//     let message
//     if (typeof params === 'string') message = params

//     const defaultParam = {
//       type: 'is-success',
//       position: 'is-bottom-right',
//       message,
//     }
//     const propsData = Object.assign(defaultParam, params)

//     const vm = typeof window !== 'undefined' && window.Vue ? window.Vue : Vue
//     const SnackbarComponent = vm.extend(Snackbar)
//     return new SnackbarComponent({
//       el: document.createElement('div'),
//       propsData,
//     })
//   },
// }

// Vue.component('BAutocomplete', Autocomplete)
Vue.component('BCheckbox', Checkbox)
// Vue.component('BCheckboxButton', CheckboxButton)
// Vue.component('BCollapse', Collapse)
// Vue.component('BDatepicker', Datepicker)
// Vue.component('BDropdown', Dropdown)
// Vue.component('BDropdownItem', DropdownItem)
Vue.component('BField', Field)
Vue.component('BIcon', Icon)
Vue.component('BInput', Input)
Vue.component('BLoading', Loading)
// Vue.component('BMessage', Message)
// Vue.component('BModal', Modal)
// Vue.component('BNotification', Notification)
// Vue.component('BPagination', Pagination)
// Vue.component('BRadio', Radio)
// Vue.component('BRadioButton', RadioButton)
Vue.component('BSelect', Select)
// Vue.component('BSwitch', Switch)
// Vue.component('BTabItem', TabItem)
// Vue.component('BTable', Table)
// Vue.component('BTableColumn', TableColumn)
// Vue.component('BTabs', Tabs)
// Vue.component('BTag', Tag)
// Vue.component('BTaginput', Taginput)
// Vue.component('BTaglist', Taglist)
// Vue.component('BTimepicker', Timepicker)
// Vue.component('BTooltip', Tooltip)
// Vue.component('BUpload', Upload)

export default (ctx, inject) => {
  // inject('dialog', DialogProgrammatic)
  // inject('spinner', LoadingProgrammatic)
  // inject('modal', ModalProgrammatic)
  // inject('snackbar', SnackbarProgrammatic)
  inject('toast', ToastProgrammatic)
}
