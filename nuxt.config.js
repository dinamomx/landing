/* eslint-disable import/no-extraneous-dependencies */

import PurgecssPlugin from 'purgecss-webpack-plugin'
import glob from 'glob-all'
import path from 'path'

const meta = [
  {
    name: 'keywords',
    /* eslint-disable max-len */
    content:
      'Tradición, talavera, pewter, vidrio soplado, productos gourmet, regalos navideños',
  },
  { name: 'google-site-verification', content: '' },
]

const link = [
  {
    href: 'https://fonts.googleapis.com/css?family=Quicksand|Rubik:400,700',
    rel: 'stylesheet',
  },
]

export default {
  /*
  ** Headers of the page
  */
  head: {
    meta,
    link,
    titleTemplate: titleChunk =>
      titleChunk ? `${titleChunk} - Darila México` : 'Darila México',
  },
  /**
   * Opciones de vue-router
   */
  router: {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-active--exact',
    middleware: [],
  },
  /**
   * Transición por defecto
   */
  transition: 'page',
  /**
   * CSS global
   */
  css: [
    '~assets/sass/global.scss',
    '~assets/sass/transitions.scss',
    '~assets/sass/app.scss',
    // NOTE: Aquí puedes modificar el estilo de fontawesome
    '@fortawesome/fontawesome-pro/css/fontawesome.css',
    '@fortawesome/fontawesome-pro/css/light.css',
    '@fortawesome/fontawesome-pro/css/brands.css',
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /**
   * Nuxt Plugins
   */
  plugins: [
    '~plugins/buefy.js',
    { ssr: false, src: '~plugins/smooth-scroll-poly.js' },
  ],
  /*
  ** Build configuration
  */
  build: {
    babel: {
      plugins: [
        // Reduce drásticamente el tamaño del bundle
        'lodash',
        '@babel/plugin-syntax-dynamic-import',
      ],
    },
    // Hace el css cacheable
    extractCSS: true,
    // Inserta base/basic en todos lados
    styleResources: {
      scss: './assets/sass/base/_basic.scss',
      sass: './assets/sass/base/_basic.scss',
    },
    // Plugins para postcss
    /**
     * @param {Object} config The Build Config
     * @param {Object} ctx Nuxt Context
     */
    extend(config, { isDev, isClient }) {
      config.module.rules.push({
        resourceQuery: /blockType=docs/,
        loader: require.resolve('./utils/docs-loader.js'),
      })
      if (!isDev) {
        // Remove unused CSS using purgecss. See https://github.com/FullHuman/purgecss
        // for more information about purgecss.
        config.plugins.push(
          new PurgecssPlugin({
            paths: glob.sync([
              path.join(__dirname, './pages/**/*.vue'),
              path.join(__dirname, './layouts/**/*.vue'),
              path.join(__dirname, './components/**/*.vue'),
            ]),
            whitelist: [
              'html',
              'body',
              'input',
              'help',
              'help.is-danger',
              'input.is-danger',
              'fal',
              'fa-exclamation-circle',
              'fa-lg',
              'has-text-danger',
              'has-icons-right',
            ],
            // whitelistPatterns: [/loading-overlay/],
            whitelistPatternsChildren: [
              /loading-overlay$/,
              /fa-exclamation-circle$/,
              /has-icons-right$/,
            ],
            // keyframes: ['spinAround'],
          })
        )
      }
      // Run ESLINT on save
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules(?!\/buefy)$/,
        })
      }
    },
  },

  /**
   * Estos modulos son altamente recomendado en producción ya que facilitan
   * la incluisión de varias herramientas
   */
  modules: [
    /**
     * PWA Este módulo integra todo lo necesario para implementar las
     * capacidades PWA a una página web, require ssl
     */
    [
      '@nuxtjs/pwa',
      {
        workbox: {
          generateSW: true,
          // importScripts: ['/notifications-worker.js'],
          InjectManifest: true,
        },
      },
    ],
  ],
  /**
   * Configuraciones que generan automáticamente manifest y
   * etiquetas básicas para seo
   */
  manifest: {
    name: 'Darila México',
    short_name: 'Darila',
  },
  meta: {
    name: 'Darila México',
    author: 'Dinamo',
    description: 'Promociones',
    theme_color: '#f83f28',
    lang: 'es_MX',
    nativeUI: false,
    mobileApp: true,
    appleStatusBarStyle: 'default',
    ogSiteName: true,
    ogTitle: true,
    ogDescription: true,
    ogImage:
      'https://promociones.darilamexico.com/images/caballitos-de-vidrio-con-tequila_medium.jpg',
    ogHost: 'https://promociones.darilamexico.com',
    ogUrl: true,
    twitterCard: true,
    twitterSite: 'https://promociones.darilamexico.com',
    twitterCreator: '@darilamexico',
  },
}
