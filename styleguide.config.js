import path from 'path'
import { Nuxt, Builder } from 'nuxt'
import PerfLoader from 'nuxt/lib/builder/webpack/utils/perf-loader'
import ClientWebpackConfig from 'nuxt/lib/builder/webpack/client'
import config from './nuxt.config'

const nuxt = new Nuxt(config)
const builder = new Builder(nuxt)

builder.perfLoader = new PerfLoader(builder.options)
const clientConfig = new ClientWebpackConfig(builder).config()

const webpackConfig = {
  resolve: clientConfig.resolve,
  module: clientConfig.module,
  // resolveLoader: clientConfig.resolveLoader,
  plugins: clientConfig.plugins,
}

module.exports = {
  require: [
    path.join(__dirname, 'styleguide.global.requires.js'),
    path.join(__dirname, 'assets/sass/app.scss'),
  ],
  components: 'components/**/*.vue',
  webpackConfig,
  getComponentPathLine(componentPath) {
    const name = path.basename(componentPath, '.vue')
    const dir = path.dirname(componentPath)
    return `import ${name} from '~/${dir}/${name}';`
  },
  showUsage: true,

  vuex: path.resolve(__dirname, 'store/index'),
}
