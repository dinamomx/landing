/* eslint-disable max-len */

module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   * Importante: Cambiar el puerto de acuerdo a la configuración de nginx
   */
  apps: [
    // First application
    {
      name: 'plantilla.dinamo.mx',
      script: 'node_modules/nuxt/bin/nuxt-start',
      env: {
        HOST: 'localhost',
        NODE_ENV: 'production',
        API_URL: 'http://localhost:3000',
      },
      env_staging: {
        PORT: 3000,
        DEBUG: 'nuxt*,din*,app*',
      },
      env_production: {
        PORT: 3000,
        PRODUCTION: true,
        DEBUG: null,
      },
    },
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy: {
    production: {
      user: 'webdev',
      host: 'droplet.dinamo.mx',
      ref: 'origin/master',
      repo: 'git@bitbucket.org:dinamomx/dinamo-nuxt.git',
      path: '/var/www/plantilla.dinamo.mx',
      'pre-deploy-local':
        'PORT=3000 HOST=localhost NODE_ENV=production yarn build; node uploadbuild.js',
      'pre-deploy': 'yarn install --frozen-lockfile --prod --non-interactive',
      'post-deploy': 'pm2 startOrReload ecosystem.config.js --env production',
    },
    staging: {
      user: 'webdev',
      host: 'droplet.dinamo.mx',
      ref: 'origin/develop',
      repo: 'git@bitbucket.org:dinamomx/dinamo-nuxt.git',
      path: '/var/www/plantilla.dinamo.mx',
      'pre-deploy-local':
        'PORT=3000 HOST=localhost NODE_ENV=production yarn build; node uploadbuild.js',
      'pre-deploy': 'yarn install --prod --frozen-lockfile --non-interactive',
      'post-deploy': 'pm2 startOrReload ecosystem.config.js --env staging',
    },
  },
}
