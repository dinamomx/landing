import { throttle } from 'lodash'

/**
 * Monitorea el nivel de scroll del usuario y añade un evento
 * de analíticas cuando sobrepasa la mitad de la página
 *
 * @mixin
 */
export default {
  data: () => ({
    // El alto de la página
    currentWindowHeight: 0,
  }),
  watch: {
    // Resetea el alto total de la página cada que se navega
    $route() {
      if (process.browser) {
        this.currentWindowHeight = window.innerHeight
      }
    },
  },
  mounted() {
    // Inicia el scrollWatcher un "tick"
    // después para asegurarse de que todo esté montado
    this.$nextTick(this.initScrollWatcher())
  },
  beforeDestroy() {
    if (process.browser) {
      // Siempre eliminar el evento al quitar el componente
      window.removeEventListener('scroll', throttle(this.scrollWatcher, 500))
    }
  },
  methods: {
    /**
     * Empieza a monitorear el scroll de la página
     */
    initScrollWatcher() {
      this.currentWindowHeight = window.innerHeight
      window.addEventListener('scroll', throttle(this.scrollWatcher, 500))
    },
    /**
     * Aplica la diferencia del alto de la pantalla con el scroll actual
     * y envía un evento de anaíticas cuando se sobrepasa la mitad
     */
    scrollWatcher() {
      // const currentY = Math.floor(window.scrollY)
      // const percentage = this.currentWindowHeight / currentY
      // console.log({ currentY, percentage })
      // if (currentY / 2 > this.currentWindowHeight) {
      // this.$ga.event(
      //   'user-tracking',
      //   document.title,
      //   'Scroll Depth',
      //   currentY
      // )
      // }
    },
  },
}
