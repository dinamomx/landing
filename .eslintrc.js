module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2017,
    sourceType: 'module'
  },
  extends: [
    'airbnb-base',
    'prettier',
    'plugin:prettier/recommended',
    'prettier/react',
    'plugin:vue/recommended'
  ],
  // required to lint *.vue files
  plugins: [
    'prettier',
    'vue',
    // 'html'
  ],
  settings: {
    'import/resolver': {
      webpack: 'webpack.config.js',
    }
  },
  // add your custom rules here
  rules: {
    semi: ['error', 'never'],
    'vue/html-closing-bracket-newline': ['error', {
      'singleline': 'never',
      'multiline': 'never'
    }],
    'vue/html-indent': ['error', 2, {
      'attribute': 1,
      'closeBracket': 0,
      'alignAttributesVertically': true,
      'ignores': []
    }],
    'vue/max-attributes-per-line': [2, {
      'singleline': 1,
      'multiline': {
        'max': 1,
        'allowFirstLine': true
      }
    }],
    'vue/component-name-in-template-casing': ['error',
      'kebab-case',
    ],
    'no-console': 1,
    'no-debugger': 1,
    'import/extensions': ['error', 'always', {
      js: 'never',
      vue: 'always'
    }],
    'max-len': ['error', {
      'code': 80,
      'ignoreUrls': true
    }],
    'no-plusplus': ['error', {
      'allowForLoopAfterthoughts': true
    }],
    'no-param-reassign': ['error', {
      'props': false
    }],
    'no-use-before-define': 0,
  }
}
