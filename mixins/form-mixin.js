/* eslint-disable no-console */
import axios from 'axios'

/**
 * Mixin de formulario genérico
 * Tiene el objetivo de permitir la reusabilidad
 * de la funcionalidad de envio de leads
 *
 * @mixin FormMixin
 * @version 1.7
 *
 * En la versión 1.7 se espera que el componente de el feedback o rediriga una
 * landing page
 */

export default {
  props: {
    /**
     * Url de envío
     */
    endpoint: {
      type: String,
      default:
        'https://script.google.com/a/wdinamo.com/macros/s/AKfycbyNabdQ1hPIUdc948cpv1E9p6V-IKEK99QueWQbKA/exec',
    },
    /**
     * Título del mail
     */
    mailTitle: {
      type: String,
      default: 'Lead Promociones Darila',
    },
    /**
     * Hoja del drive a usar
     */
    sheetDestiny: {
      type: String,
      default: 'Leads',
    },
    /**
     * Orden en el que se muestran los datos en el correo,
     *   lo que no esté incluido aquí es omitido
     */
    formDataOrder: {
      type: [Array, String],
      default() {
        return ['Fecha', 'Nombre', 'Teléfono', 'Correo', 'Empresa']
      },
    },
    /**
     * Correo al que se envía este lead
     */
    sendTo: {
      type: String,
      // default: 'mesquivel@darilamexico.com',
      default: 'cesar@wdinamo.com',
    },
    /**
     * Correo al que las respuestas son enviadas
     */
    replyTo: {
      type: String,
      default: '',
    },
    /**
     * Asunto del correo
     */
    subject: {
      type: String,
      default: 'Formulario de contacto en Promociones Darila',
    },
    /**
     * Nombre del remitente
     */
    senderName: {
      type: String,
      default: 'Darila',
    },
    /**
     * Con copia
     */
    copyTo: {
      type: String,
      default: '',
    },
    /**
     * Copia oculta
     */
    ocultCopyTo: {
      type: String,
      default: '',
    },
    /**
     * Correo al que se envía las notificaciones de fallos.
     */
    debugMail: {
      type: String,
      default: 'cesar@wdinamo.com',
    },
  },
  data: () => ({
    // Estado de carga
    isLoading: false,
    // Bloqueo del botón y evento de envios
    preventSending: false,
    // Envío exitoso
    success: false,
    // Envío fallo
    failed: false,
    // Campo de terminos de privacidad
    TOS: false,
    // Modelo del formulario
    model: {
      Fecha: '',
      Nombre: '',
      Teléfono: '',
      Correo: '',
    },
  }),
  computed: {
    /**
     * El objeto a enviarse, combina las props con el objeto del formulario
     *
     * @returns {Object} payload
     */
    payload() {
      return Object.assign(
        {
          sheetDestiny: this.sheetDestiny,
          formDataOrder: this.formDataOrder,
          sendTo: this.sendTo,
          replyTo: this.replyTo,
          subject: this.subject,
          debugMail: this.debugMail,
          senderName: this.senderName,
          ocultCopyTo: this.ocultCopyTo,
          copyTo: this.copyTo,
          mailTitle: this.mailTitle,
        },
        this.model
      )
    },
  },
  mounted() {
    const today = new Date()
    this.model.Fecha = today.toLocaleString('es')
  },
  methods: {
    /**
     * Procesa el resultado de la respuesta
     *
     * @param {Object} response La respuesta del script de gdocs
     * @param {Object} response.result El resultado del script
     * @param {Object} response.mailResult.status El resultado del envío
     *   de correo
     * @param {Object} response.sheet.status El resultado de guardado del correo
     */
    handleResponse(response) {
      if (response && response.result) {
        this.isSuccess()
      } else if (response && response.mailResult.status) {
        console.warn('No se guardó en el drive, pero se envió al correo')
        this.isSuccess()
      } else {
        console.error('Algo malo sucede')
        this.isError()
      }
      this.isLoading = false
      setTimeout(() => {
        this.preventSending = false
      }, 5000)
    },
    /**
     * Callback para mostrar resultado de éxito
     */
    isSuccess() {
      /**
       * Evento de envio exitoso, se puede usar para cerrar modales
       *
       * @event success
       * @type {Event}
       */
      this.$emit('success', this.payload)
      this.resetForm()
    },
    /**
     * Callback para mostrar error
     */
    isError() {
      window.gtag('event', 'generate_lead', {
        event_label: 'error',
      })
      this.$toast.open({
        duration: 5000,
        message: 'Hubo un error enviando tu solicitud. Intentalo más tarde',
        position: 'is-bottom',
        type: 'is-danger',
      })
      /**
       * Evento de error enviando, se puede usar para cerrar modales
       *
       * @event error
       * @type {Event}
       */
      this.$emit('error')
    },
    /**
     * Resetea los datos del formulario
     */
    resetForm() {
      const today = new Date()
      Object.keys(this.model).forEach(key => {
        if (this.payload[key] instanceof Date) {
          this.model[key] = new Date()
        } else {
          this.model[key] = ''
        }
      })
      this.model.Fecha = today.toLocaleString('es')
      // this.$el.reset()
    },
    /**
     * Convierte los datos del formulario en FormData
     *
     * @returns {FormData} formData
     */
    formatPayload() {
      const formData = new FormData()
      Object.keys(this.payload).forEach(key => {
        if (this.payload[key] instanceof Date) {
          formData.append(key, this.payload[key].toLocaleString('es'))
        } else if (typeof this.payload[key] === 'object') {
          formData.append(key, JSON.stringify(this.payload[key]))
        } else if (this.payload[key]) {
          formData.append(key, this.payload[key])
        }
      })
      return formData
    },
    /**
     * Envía el formulario al script de gdocs
     */
    formSend() {
      if (!this.TOS) {
        this.$toast.open({
          duration: 5000,
          message: 'Por favor acepta nuestro aviso de privacidad.',
          position: 'is-bottom',
          type: 'is-danger',
        })
        return
      }
      this.isLoading = true
      this.preventSending = true
      const vm = this
      const formData = this.formatPayload()
      axios
        .post(this.endpoint, formData, {
          headers: {
            'Content-Type': 'application/formdata',
          },
        })
        .then(({ data }) => {
          vm.handleResponse(data)
        })
        .catch(er => {
          console.error(er)
          vm.isLoading = false
          vm.preventSending = false
          vm.isError()
        })
    },
  },
}
