/** ***************************************************************************\

 Javascript "SOAP Client" library

 @version: 2.4 - 2007.12.21
 @author: Matteo Casati - http://www.guru4.net/

\**************************************************************************** */

export const SOAPClientParameters = class SOAPClientParameters {
  constructor() {
    const pL = []
    this.add = function add(name, value) {
      pL[name] = value
      return this
    }
    this.toXml = () => {
      let xml = ''
      pL.forEach(p => {
        switch (typeof p) {
          case 'string':
          case 'number':
          case 'boolean':
          case 'object':
            xml += `<${p}>${SOAPClientParameters.serialize(pL[p])}</${p}>`
            break
          default:
            break
        }
      })
      return xml
    }
  }

  static serialize(o) {
    let s = ''
    switch (typeof o) {
      case 'string':
        s += o
          .replace(/&/g, '&amp;')
          .replace(/</g, '&lt;')
          .replace(/>/g, '&gt;')
        break
      case 'number':
      case 'boolean':
        s += o.toString()
        break
      case 'object':
        // Date
        if (o instanceof Date) {
          s += o.toISOString()
        }
        // Array
        else if (Array.isArray(o)) {
          o.forEach(p => {
            if (!Number.isNaN(p)) {
              // linear array
              ;/function\s+(\w*)\s*\(/gi.exec(o[p].constructor.toString())
              let type = RegExp.$1
              switch (type) {
                case '':
                  type = typeof o[p]
                  break
                case 'String':
                  type = 'string'
                  break
                case 'Number':
                  type = 'int'
                  break
                case 'Boolean':
                  type = 'bool'
                  break
                case 'Date':
                  type = 'DateTime'
                  break
                default:
                  console.log('Unknown type', type)
              }
              s += `<${type}>${SOAPClientParameters.serialize(o[p])}</${type}>`
            } // associative array
            else s += `<${p}>${SOAPClientParameters.serialize(o[p])}</${p}>`
          })
        }
        // Object or custom function
        else
          Object.keys(o).forEach(key => {
            s += `<${key}>${SOAPClientParameters.serialize(o[key])}</${key}>`
          })
        break
      default:
        throw new Error(
          500,
          `SOAPClientParameters: type '${typeof o}' is not supported`
        )
    }
    return s
  }
}

export const SOAPClient = class SOAPClient {
  constructor(url, method, headerParams, parameters, callback) {
    this.url = url
    this.method = method
    this.headerParams = headerParams
    this.parameters = parameters
    this.callback = callback
    // load from cache?
    this.wsdl = cacheWsdl[url]
    if (this.wsdl) {
      return this.sendSoapRequest()
    }
    // get wsdl
    const xmlHttp = new XMLHttpRequest()
    xmlHttp.open('GET', `${url}?wsdl`, true)
    xmlHttp.onreadystatechange = () => {
      if (xmlHttp.readyState === 4) SOAPClient.onLoadWsdl(url, xmlHttp)
    }
    xmlHttp.send(null)
  }

  static onLoadWsdl(url, req) {
    this.wsdl = req.responseXML
    cacheWsdl[url] = this.wsdl // save a copy in cache
    return this.sendSoapRequest()
  }

  sendSoapRequest() {
    const { url, method, headerParams, parameters, wsdl } = this
    // get namespace
    const ns =
      `${wsdl.documentElement.attributes.targetNamespace}` === 'undefined'
        ? wsdl.documentElement.attributes.getNamedItem('targetNamespace')
            .nodeValue
        : wsdl.documentElement.attributes.targetNamespace.value
    // build SOAP request
    const sr =
      '<?xml version="1.0" encoding="utf-8"?>' +
      '<soap:Envelope ' +
      'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
      'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ' +
      'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
      `<soap:Header>'}${headerParams.toXml()}</soap:Header>` +
      '<soap:Body>' +
      `<${method} xmlns="${ns}">${parameters.toXml()}</${method}>` +
      '</soap:Body></soap:Envelope>'
    const xmlHttp = SOAPClient.getXmlHttp()
    if (SOAPClient.userName && SOAPClient.password) {
      xmlHttp.open('POST', url, true, SOAPClient.userName, SOAPClient.password)
      // Some WS implementations (i.e. BEA WebLogic Server 10.0 JAX-WS)
      // don't support Challenge/Response HTTP BASIC, so we send authorization
      //  headers in the first request
      xmlHttp.setRequestHeader(
        'Authorization',
        `Basic ${SOAPClient.toBase64(
          `${SOAPClient.userName}:${SOAPClient.password}`
        )}`
      )
    } else xmlHttp.open('POST', url, true)
    const soapaction =
      (ns.lastIndexOf('/') !== ns.length - 1 ? `${ns}/` : ns) + method
    xmlHttp.setRequestHeader('SOAPAction', soapaction)
    xmlHttp.setRequestHeader('Content-Type', 'text/xml; charset=utf-8')
    xmlHttp.onreadystatechange = () => {
      if (xmlHttp.readyState === 4) SOAPClient.onSendSoapRequest(xmlHttp)
    }
    xmlHttp.send(sr)
  }

  static onSendSoapRequest(req) {
    let o = null
    let nd = SOAPClient.getElementsByTagName(
      req.responseXML,
      `${this.method}Result`
    )
    if (nd.length === 0)
      // PHP web Service?
      nd = SOAPClient.getElementsByTagName(req.responseXML, 'return')
    if (nd.length === 0) {
      if (req.responseXML.getElementsByTagName('faultcode').length > 0) {
        if (this.callback)
          o = new Error(
            500,
            req.responseXML.getElementsByTagName(
              'faultstring'
            )[0].childNodes[0].nodeValue
          )
        else
          throw new Error(
            500,
            req.responseXML.getElementsByTagName(
              'faultstring'
            )[0].childNodes[0].nodeValue
          )
      }
    } else o = SOAPClient.soapresult2object(nd[0], this.wsdl)
    if (this.callback) this.callback(o, req.responseXML)
  }

  static soapresult2object(node, wsdl) {
    const wsdlTypes = SOAPClient.getTypesFromWsdl(wsdl)
    return SOAPClient.node2object(node, wsdlTypes)
  }

  static node2object(node, wsdlTypes) {
    // null node
    if (node === null) return null
    // text node
    if (node.nodeType === 3 || node.nodeType === 4)
      return SOAPClient.extractValue(node, wsdlTypes)
    // leaf node
    if (
      node.childNodes.length === 1 &&
      (node.childNodes[0].nodeType === 3 || node.childNodes[0].nodeType === 4)
    )
      return SOAPClient.node2object(node.childNodes[0], wsdlTypes)
    const isarray = SOAPClient.getTypeFromWsdl(node.nodeName, wsdlTypes)
      .toLowerCase()
      .includes('arrayof')
    // object node
    if (!isarray) {
      let obj = null
      if (node.hasChildNodes()) obj = {}
      node.childNodes.forEach(childNode => {
        const p = SOAPClient.node2object(childNode, wsdlTypes)
        obj[childNode.nodeName] = p
      })
      return obj
    }
    // list node

    // create node ref
    const l = []
    node.childNodes.forEach(childNode => {
      l[l.length] = SOAPClient.node2object(childNode, wsdlTypes)
    })
    return l
  }

  static extractValue(node, wsdlTypes) {
    let value = node.nodeValue
    switch (
      SOAPClient.getTypeFromWsdl(
        node.parentNode.nodeName,
        wsdlTypes
      ).toLowerCase()
    ) {
      default:
      case 's:string':
        return value != null ? `${value}` : ''
      case 's:boolean':
        return `${value}` === 'true'
      case 's:int':
      case 's:long':
        return value != null ? parseInt(`${value}`, 10) : 0
      case 's:double':
        return value != null ? parseFloat(`${value}`) : 0
      case 's:datetime':
        // eslint-disable-next-line
        const d = new Date()
        if (value === null) return null
        value += ''
        value = value.substring(
          0,
          value.lastIndexOf('.') === -1 ? value.length : value.lastIndexOf('.')
        )
        value = value.replace(/T/gi, ' ')
        value = value.replace(/-/gi, '/')
        d.setTime(Date.parse(value))
        return d
    }
  }

  static getTypesFromWsdl(wsdl) {
    const wsdlTypes = []
    // IE
    let ell = wsdl.getElementsByTagName('s:element')
    let useNamedItem = true
    // MOZ
    if (ell.length === 0) {
      ell = wsdl.getElementsByTagName('element')
      useNamedItem = false
    }
    for (let i = 0; i < ell.length; i++) {
      if (useNamedItem) {
        if (
          ell[i].attributes.getNamedItem('name') != null &&
          ell[i].attributes.getNamedItem('type') != null
        )
          wsdlTypes[ell[i].attributes.getNamedItem('name').nodeValue] = ell[
            i
          ].attributes.getNamedItem('type').nodeValue
      } else if (
        ell[i].attributes.name != null &&
        ell[i].attributes.type != null
      )
        wsdlTypes[ell[i].attributes.name.value] = ell[i].attributes.type.value
    }
    return wsdlTypes
  }

  static getTypeFromWsdl(elementname, wsdlTypes) {
    const type = `${wsdlTypes[elementname]}`
    return type === 'undefined' ? '' : type
  }

  // private: utils
  static getElementsByTagName(document, tagName) {
    try {
      // trying to get node omitting any namespaces
      // (latest versions of MSXML.XMLDocument)
      return document.selectNodes(`.//*[local-name()="${tagName}"]`)
    } catch (ex) {
      // old XML parser support
      console.log(ex)
    }
    // xmlDoc.evaluate(xpath, xmlDoc, null, XPathResult.ANY_TYPE,null);
    return document.getElementsByTagName(tagName)
  }

  // private: xmlhttp factory
  static getXmlHttp() {
    return new XMLHttpRequest()
  }

  /* eslint-disable no-bitwise */
  /* eslint-disable no-plusplus */
  static toBase64(input) {
    const keyStr =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
    let output = ''
    let chr1
    let chr2
    let chr3
    let enc1
    let enc2
    let enc3
    let enc4
    let i = 0

    do {
      chr1 = input.charCodeAt(i++)
      chr2 = input.charCodeAt(i++)
      chr3 = input.charCodeAt(i++)

      enc1 = chr1 >> 2
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4)
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6)
      enc4 = chr3 & 63

      if (Number.isNaN(chr2)) {
        // eslint-disable-next-line
        enc3 = enc4 = 64
      } else if (Number.isNaN(chr3)) {
        enc4 = 64
      }

      output =
        output +
        keyStr.charAt(enc1) +
        keyStr.charAt(enc2) +
        keyStr.charAt(enc3) +
        keyStr.charAt(enc4)
    } while (i < input.length)

    return output
  }
  /* eslint-enable no-bitwise */
}

SOAPClient.username = null
SOAPClient.password = null

// private: wsdl cache
const cacheWsdl = []

export default { SOAPClient, SOAPClientParameters }
