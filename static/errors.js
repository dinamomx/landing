/* eslint-disable */
'use strict'

window.isInternetExplorer = !!window.MSInputMethodContext && !!document.documentMode
window.failed = false

if (window.isInternetExplorer) {
  document.getElementById('__old-browser-alert').style.display = 'block'
}
function handleErrors(errorMessage, e, url, line) {
  var errorBlock = document.getElementById('__general-error')
  var debugText = JSON.stringify({
    message: errorMessage,
    Surl: url,
    Dline: line,
  })
  errorBlock.style.display = 'block'
  console.error({
    message: errorMessage,
    data: e,
    Surl: url,
    Dline: line,
  })
  if (typeof ga !== 'function') {
    /* eslint-disable */
    (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments);
      }, i[r].l = 1 * new Date();a = s.createElement(o), m = s.getElementsByTagName(o)[0];a.async = 1;a.src = g;m.parentNode.insertBefore(a, m);
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-143980-7', 'auto')
  }
  ga('send', {
    hitType: 'event',
    eventCategory: 'Exceptions',
    eventAction: errorMessage,
    eventLabel: debugText,
  })
  ga('send', 'exception', {
    exDescription: errorMessage,
    exFatal: true,
  })
}
function checkIfNuxtIsOk() {
  setTimeout(function () {
    var errorMessage
    if (typeof window.$nuxt === 'undefined' || !window.$nuxt.$root._isMounted) {
      errorMessage = '$NUXT no se montó correctamente'
      handleErrors(errorMessage, window.$nuxt, document.URL.toString(), 0)
    } else if (__NUXT__.error) {
      errorMessage = 'NUXT tuvo un fallo pero no se que es'
      handleErrors(errorMessage, __NUXT__.error, document.URL.toString(), 0)
    }
  }, 10 * 1000);
}
window.addEventListener('load', checkIfNuxtIsOk)
