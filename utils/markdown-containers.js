/**
 * Usando opciones de clases y otros atributos
 * http://pandoc.org/MANUAL.html#extension-header_attributes
 */

// Usado en conjunto con markdown-it-attrs
/**
 * Merge attributes from markdownItAttrs
 * @param {String[][]|null} attrs Array of attributes
 * @param {String} defaultClass La clase por defecto
 */
const mergeAttrs = (attrs, defaultClass) => {
  if (!attrs) {
    return `class="${defaultClass}"`
  }
  let result = ''
  attrs.forEach(attribute => {
    const [attributeName] = attribute
    const values = attribute.slice(1)
    if (attributeName === 'class') {
      // Clase por defecto
      values.push(defaultClass)
    }
    // Añadiendo un espacio si ya hay atributos
    if (result.length > 0) {
      result += ' '
    }
    result += `${attributeName}`
    // Si tiene valores el atribito, ponlos
    if (values.length > 0) {
      result += '="'
      result += values.join(' ')
      result += '"'
    }
  })
  return result
}

const section = {
  render(tokens, idx) {
    if (tokens[idx].nesting >= 1) {
      const { attrs } = tokens[idx]
      let stringAttrs = ''
      stringAttrs = mergeAttrs(attrs, 'section')
      // Opening tag
      return `<section ${stringAttrs}>`
    }
    // closing tag
    return '</section>'
  },
}

const column = {
  render(tokens, idx) {
    if (tokens[idx].nesting >= 1) {
      const { attrs } = tokens[idx]
      let stringAttrs = ''
      stringAttrs = mergeAttrs(attrs, 'column')
      // Opening tag
      return `<div ${stringAttrs}>`
    }
    // closing tag
    return '</div>'
  },
}

const columns = {
  render(tokens, idx) {
    if (tokens[idx].nesting >= 1) {
      const { attrs } = tokens[idx]
      let stringAttrs = ''
      stringAttrs = mergeAttrs(attrs, 'columns')
      // Opening tag
      return `<div ${stringAttrs}>`
    }
    // closing tag
    return '</div>'
  },
}

const notification = {
  render(tokens, idx) {
    if (tokens[idx].nesting >= 1) {
      const { attrs } = tokens[idx]
      let stringAttrs = ''
      stringAttrs = mergeAttrs(attrs, 'notification')
      // Opening tag
      return `<div ${stringAttrs}>`
    }
    // closing tag
    return '</div>'
  },
}

module.exports = {
  section,
  column,
  columns,
  notification,
}
